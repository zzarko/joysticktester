//  Author:Fred.Chu
//  Date:14 August, 2014
//
//  Applicable Module:
//                     Battery Display v1.0
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//Modified record:
//Autor:    Zarko Zivanov
//Mail:     zzarko@lugons.org
//IDE:      Arduino-1.8.5
//Typ.:     Gotek TM1651-based 3-digit 7-segment display
//Date:     26.12.2017
//Parts of code copied from FlashFloppy project:
//   https://github.com/keirf/FlashFloppy/blob/master/src/gotek/led_7seg.c

#include "GotekTM1651.h"
#include <Arduino.h>

/* TM1651: Alphanumeric segment arrangements.  */
static const uint8_t letters[] = {
    0x77, 0x7c, 0x58, 0x5e, 0x79, 0x71, 0x6f, 0x74, 0x06, // A B C D E F G H I
    0x0e, 0x00, 0x38, 0x37, 0x54, 0x5c, 0x73, 0x67, 0x50, // J * L M N O P Q R
    0x6d, 0x78, 0x1c, 0x72, 0x3e, 0x76, 0x6e, 0x00        // S T U W V X Y *
};

static const uint8_t digits[] = {
    0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f
};

GotekTM1651::GotekTM1651(uint8_t Clk, uint8_t Data) {
    Clkpin = Clk;
    Datapin = Data;
    pinMode(Clkpin, OUTPUT);
    pinMode(Datapin, OUTPUT);
}

void GotekTM1651::init() {
    set(BRIGHT_TYPICAL);

    start();
    writeByte(ADDR_AUTO);
    stop();

    start();
    writeByte(0xc1);
    for(uint8_t i=0; i<3; i++)
        writeByte(0x40);
    stop();

    start();
    writeByte(Cmd_DispCtrl);
    stop();

    clearDisplay();
}

bool GotekTM1651::writeByte(int8_t wr_data) {
    uint8_t i,count1=0;
    for(i=0; i<8; i++) //send 8bit data
    {
        digitalWrite(Clkpin, LOW);
        if (wr_data & 0x01)
            digitalWrite(Datapin, HIGH); //LSB first
        else
            digitalWrite(Datapin, LOW);
        delayMicroseconds(3);
        wr_data >>= 1;
        digitalWrite(Clkpin, HIGH);
        delayMicroseconds(3);
    }
    digitalWrite(Clkpin, LOW); //wait for the ACK
    digitalWrite(Datapin, HIGH);
    digitalWrite(Clkpin, HIGH);
    pinMode(Datapin, INPUT);
    while(digitalRead(Datapin))
    { 
        delayMicroseconds(3);
        count1++;
        if(count1 == 5) return false;
        pinMode(Datapin, OUTPUT);
        digitalWrite(Datapin, LOW);
        pinMode(Datapin, INPUT);
    }
    pinMode(Datapin, OUTPUT);
    return true;
}

//send start signal to TM1651
void GotekTM1651::start(void) {
    digitalWrite(Clkpin, HIGH);
    digitalWrite(Datapin, HIGH);
    delayMicroseconds(2);
    digitalWrite(Datapin, LOW);
    digitalWrite(Clkpin, LOW);
} 

//End of transmission
void GotekTM1651::stop(void) {
    digitalWrite(Clkpin, LOW);
    // delayMicroseconds(2);
    digitalWrite(Datapin, LOW);
    //  delayMicroseconds(2);
    digitalWrite(Clkpin, HIGH);
    // delayMicroseconds(2);
    digitalWrite(Datapin, HIGH); 
}

void GotekTM1651::clearDisplay(void) {
  writeString("");
}

//These values take effect the on next display call
void GotekTM1651::set(uint8_t brightness,uint8_t SetData,uint8_t SetAddr) {
    Cmd_SetData = SetData;
    Cmd_SetAddr = SetAddr;
    Cmd_DispCtrl = 0x88 + brightness; //Set the brightness
}

void GotekTM1651::writeData(uint8_t d1, uint8_t d2, uint8_t d3) {
    bool ok = true;
    start();
    if (!writeByte(0xc0)) {ok = false;};
    if (!writeByte(d1)) {ok = false;};
    if (!writeByte(d2)) {ok = false;};
    if (!writeByte(d3)) {ok = false;};
    if (!writeByte(0)) {ok = false;};
    stop();
    if (!ok) { Serial.println("TM1651 LOCK"); init(); }
}

void GotekTM1651::writeString(const char *p) {
    uint8_t d[3] = { 0 }, c;
    unsigned int i;

    for (i = 0; ((c = *p++) != '\0') && (i < sizeof(d)); i++) {
        if ((c >= '0') && (c <= '9')) {
            d[i] = digits[c - '0'];
        } else if ((c >= 'a') && (c <= 'z')) {
            d[i] = letters[c - 'a'];
        } else if ((c >= 'A') && (c <= 'Z')) {
            d[i] = letters[c - 'A'];
        } else if (c == '-') {
            d[i] = 0x40;
        } else {
            d[i] = 0;
        }
    }
    writeData(d[0],d[1],d[2]);
}

void GotekTM1651::writeNumber(unsigned int val) {
    char msg[4];
    snprintf(msg, sizeof(msg), "%03u", val % 1000);
    writeString(msg);
}

