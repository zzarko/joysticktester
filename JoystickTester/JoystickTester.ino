/*

DB-9 Joystick Tester
Copyright (C) 2018  Žarko Živanov (Zarko Zivanov)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
    DB-9 Joystick pinouts
    ---------------------

    5 4 3 2 1                                   Pot  - Analog input, potentiometer
   -----------      Joystick connector view     Sel  - Select line for additional read
   \O O O O O/                                  NC   - Not connected/used
    \O O O O/                                   Fire - Fire, Button
     -------                                    UDLR - Directions
     9 8 7 6

    Type/Pin    1       2       3       4       5       6       7       8       9

    2600 Joy    Up      Down    Left    Right   NC      Fire    NC      GND     NC
    7800 Joy    Up      Down    Left    Right   FireR   Fire    VCC     GND     FireL
    Atari8bit   Up      Down    Left    Right   NC      Fire    VCC     GND     NC
    C64  Joy    Up      Down    Left    Right   NC      Fire    VCC     GND     NC/Fire2
    Amiga Joy   Up      Down    Left    Right   Fire3   Fire1   VCC     GND     Fire2
    At ST Joy   Up      Down    Left    Right   NC      Fire1   VCC     GND     Fire2

    C64  Paddle NC      NC      FireX   FireY   PotY    NC      VCC     GND     PotX
    2600 Paddle NC      NC      FireA   FireB   PotB    NC      VCC     GND     PotA
    Amiga Padd  Fire3   NC      Fire1   Fire2   PotX    NC      VCC     GND     PotY

    CPC         Up      Down    Left    Right   Fire3   Fire1   Fire2   Se1/GND Se2/VCC
    CPC         Up      Down    Left    Right   Fire3   Fire1   Fire2   Se1/VCC Se2/GND

    MSX         Up      Down    Left    Right   VCC     Fire1   Fire2   GND/NC  GND

    Sega Master Up      Down    Left    Right   VCC     Fire1   NC      GND     Fire2

    ZX+2+3      NC      GND     NC      Right   Up      Fire    Left    GND     Down

    Vectrex     Fire1   Fire2   Fire3   Fire4   PotX    PotY    VCC     GND     -VCC

    Videopac    GND     Up      Right   Down    Left    Fire    NC      NC      NC

    Amiga Mse   V-Puls  H-Puls  VQ-Puls HQ-Puls FireM   FireL   VCC     GND     FireR
    At ST Mse   XB      XA      YA      YB      NC      FireL   VCC     GND     FireR

    Apple IIc   Fire1   VCC     GND     NC      Pad0    NC      Fire0   Pad1    NC

    Sega Gen3B  Up      Down    GND     GND     VCC     FireA   Sel/GND GND     Start
    Sega Gen3B  Up      Down    Left    Right   VCC     FireB   Sel/VCC GND     FireC

    TI-99/4A    NC      Se2/VCC Up      Fire    Left    NC      Se1/GND Down    Right
    TI-99/4A    NC      Se2/GND Up      Fire    Left    NC      Se1/VCC Down    Right

    ColecoV     Up      Down    Left    Right   Se1/VCC FireL   QuadA   Se2/GND QuadB
    ColecoV     Key1    Key2    Key3    Key4    Se1/GND FireR   NC      Se2/VCC NC

    Links for non-atari standard platforms
    ------------------------------
    Sega Genesis:   http://pinouts.ru/Game/genesiscontroller_pinout.shtml
    Amstrad CPC:    http://www.cpcwiki.eu/index.php/Connector:Digital_joystick
    MSX:            http://www.konamiman.com/msx/msx2th/th-5a.txt
    TI-99/4A:       http://www.unige.ch/medecine/nouspikel/ti99/joystick.htm
    Videopac:       http://videopac.nl/forum/index.php?topic=2330.0
    ColecoVision:   http://arcarc.xmission.com/Web%20Archives/Deathskull%20%28May-2006%29/games/tech/cvcont.html
    Vectrex:        http://vectrex.playntradeonline.com/technical.html
    Many:           https://github.com/intel/mOS/blob/master/Documentation/input/joystick-parport.txt
                    http://wiki.icomp.de/wiki/DB9-Joystick
*/

/*
    PCF8574

    http://www.francois-ouellet.ca/adding-pins-to-your-arduino-using-i2c-and-a-pcf8574p-chip/
*/

#include <GotekTM1651.h>
#include <Wire.h>

/******************************
 *   Macros for debug output
 ******************************/

//#define JOYDEBUG
#ifdef JOYDEBUG
  #define dbgprint(args...) Serial.print(args)
  #define dbgprintln(args...) Serial.println(args)
#else
  #define dbgprint(args...)
  #define dbgprintln(args...)
#endif

/***************************************
 * Joystick configuration definitions
 ***************************************/

// Joystick wiring constants
enum joystick { JOYNC=0xFF, JOYUP=1, JOYDOWN, JOYLEFT, JOYRIGHT, JOYFIRE1,
                JOYFIRE2, JOYFIRE3, JOYFIRE4, JOYGND, JOYVCC, JOYPOT, JOYLAST };
/*
 * Every joustick configuration must have 9 or multiple of 9 pin definitions (JOYMAXSTATE currently seto to 2), ending with JOYLAST.
 * If joystick has select lines, in each of its 9 pin definitions they are set to JOYVCC or JOYGND, depending on select line
 * state for each combination (see for eaxmple Sega Genesis definition).
 * Potentiometers can be read only on pins 5,6,8 and 9, as they are connected to analog inputs.
 * Some of the configurations were adjusted to better suit LED layout on joystick tester.
 */

// joystick configuration for Atari, Commodore and many others
const uint8_t joyATA[] = { JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYFIRE3, JOYFIRE1, JOYVCC, JOYGND, JOYFIRE2, JOYLAST };

// joystick configuration for Amstrad CPC
const uint8_t joyCPC[] = { JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYFIRE3, JOYFIRE1, JOYFIRE2, JOYGND, JOYVCC,
                           JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYFIRE3, JOYFIRE1, JOYFIRE2, JOYVCC, JOYGND, JOYLAST };

// joystick configuration for MSX
const uint8_t joyMSX[] = { JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYVCC, JOYFIRE1, JOYFIRE2, JOYGND, JOYGND, JOYLAST };

// joystick configuration for Atari/Commodore paddle
const uint8_t joyPAD[] = { JOYFIRE3, JOYNC, JOYFIRE1, JOYFIRE2, JOYPOT, JOYNC, JOYVCC, JOYGND, JOYPOT, JOYLAST };

// joystick configuration for Amiga/Atari mouse
const uint8_t joyMSE[] = { JOYUP, JOYLEFT, JOYDOWN, JOYRIGHT, JOYFIRE2, JOYFIRE3, JOYVCC, JOYGND, JOYFIRE4, JOYLAST };

// joystick configuration for Sinclair ZX +2/+3
const uint8_t joySNC[] = { JOYNC, JOYGND, JOYNC, JOYRIGHT, JOYUP, JOYFIRE1, JOYLEFT, JOYGND, JOYDOWN, JOYLAST };

// joystick configuration for Sega Master
const uint8_t joySMS[] = { JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYVCC, JOYFIRE1, JOYNC, JOYGND, JOYFIRE2, JOYLAST };

// joystick configuration for Sega Genesis
const uint8_t joyGEN[] = { JOYUP, JOYDOWN, JOYGND,  JOYGND,   JOYVCC, JOYFIRE3, JOYGND, JOYGND, JOYFIRE1,
                           JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYVCC, JOYFIRE2, JOYVCC, JOYGND, JOYFIRE4, JOYLAST };

// joystick configuration for Vectrex
const uint8_t joyVEC[] = { JOYFIRE1, JOYFIRE3, JOYFIRE2, JOYFIRE4, JOYPOT, JOYPOT, JOYVCC, JOYGND, JOYGND, JOYLAST };

// joystick configuration for Videopac G7400
const uint8_t joy740[] = { JOYGND, JOYUP, JOYRIGHT, JOYDOWN, JOYLEFT, JOYFIRE1, JOYNC, JOYNC, JOYNC, JOYLAST };

// joystick configuration for Texas Instruments TI-99/4A
const uint8_t joyT99[] = { JOYNC, JOYVCC, JOYUP, JOYFIRE1, JOYLEFT, JOYNC, JOYGND, JOYDOWN, JOYRIGHT,
                           JOYNC, JOYGND, JOYUP, JOYFIRE1, JOYLEFT, JOYNC, JOYVCC, JOYDOWN, JOYRIGHT, JOYLAST};

// joystick configuration for ColecoVision
const uint8_t joyCOL[] = { JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYVCC, JOYFIRE2, JOYNC, JOYGND, JOYNC,
                           JOYUP, JOYDOWN, JOYLEFT, JOYRIGHT, JOYGND, JOYFIRE3, JOYNC, JOYVCC, JOYNC, JOYLAST };

// joystick configuration for Apple IIc
const uint8_t joyIIC[] = { JOYFIRE2, JOYVCC, JOYGND, JOYNC, JOYPOT, JOYNC, JOYFIRE1, JOYPOT, JOYNC, JOYLAST };

// arrays for all configurations
const uint8_t JOYCONFIGS = 13; // IMPORTANT: change if number of configurations is changed!
const uint8_t* joyconfig[] = { joyATA, joyCPC, joyMSX, joyPAD, joyMSE, joySNC, joySMS, joyGEN, joyVEC, joy740, joyT99, joyCOL, joyIIC };
const char* joyname[] = { "ATA", "CPC", "MSX", "PAD", "MSE", "SNC", "SMS", "GEN", "VEC", "740", "T99", "COL", "IIC" };

/******************************
 *          Gotek LCD
 ******************************/

// GotekTM1651 pin assignment
const uint8_t CLK_PIN = 2;
const uint8_t DIO_PIN = 3;

/******************************
 *          PCF8574
 ******************************/

// bit positions of LEDs on PCF8574
static const uint8_t joybit[] = { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 };

/******************************
 *          Buttons
 ******************************/

// button pins
const uint8_t BTN1_PIN = 10;
const uint8_t BTN2_PIN = 11;

// button debounce delay
const unsigned long debounceDelay = 50;

// button debounce
int button1State;
int lastButton1State = HIGH;
unsigned long lastDebounce1Time = 0;
int button2State;
int lastButton2State = HIGH;
unsigned long lastDebounce2Time = 0;

// button modes
enum modes { BTN1_CFGSEL, BTN1_POT, POT_LINE, POT_NUMBER };

// operation mode
uint8_t btn1Mode = BTN1_CFGSEL;
uint8_t potMode = POT_NUMBER;

/******************************
 *   Analog joystick inputs
 ******************************/

// Maximum number of potentiometers
const uint8_t JOYMAXPOT = 4;

// blinking interval for potentiometer selection
const unsigned long potBlinkInterval = 200;

// potentiometer read interval
const unsigned long potReadInterval = 100;

// potentiometer reading buffer length
const uint8_t POTBUFF = 5;

// current analog input
uint8_t joypotcurr = 0;

// min and max values for potentiometer
long joypotmin = 1023;
long joypotmax = 0;
int potbuff[POTBUFF];
uint8_t potbuffcnt = 0;

// blinking the fire3/4 LED if reading analog input
unsigned long potBlinkTime = 0;
boolean potBlinkState = true;
unsigned long potReadTime = 0;

// analog line display for Gotek LCD
static const uint8_t lcdanalog[][3] = { {0x00,0x00,0x00},
  {0x01,0x00,0x00},{0x01,0x01,0x00},{0x01,0x01,0x01},{0x01,0x01,0x03},
  {0x01,0x01,0x07},{0x01,0x01,0x0f},{0x01,0x09,0x0f},{0x09,0x09,0x0f},
  {0x19,0x09,0x0f},{0x59,0x09,0x0f},{0x59,0x49,0x0f},{0x59,0x49,0x4f}
};

/******************************
 *   General joystick data
 ******************************/

// pin mapping for joystick port
//                             1  2  3  4  5  6  7  8  9 <- DB9 Connector Positions
const uint8_t joyPinMap[9] = { 4, 5, 6, 7,A0,A1, 8,A2,A3};

// pin description for alternating lines
typedef struct {
    uint8_t pin;    // arduino pin
    uint8_t mode;   // pin mode INPUT, INPUT_PULLUP, OUTPUT
    uint8_t level;  // HIGH or LOW, if OUTPUT
    uint8_t pos;    // joystick position from enum joystick
    boolean alt;    // if this pin has alternating states
} joyLineData;

// maximum number of alternating states
const uint8_t JOYMAXSTATE = 2;

// joystick lines
joyLineData joyLine[JOYMAXSTATE][9];

// number of alternating states
uint8_t joyStates = 1;

// pin assignments for current joystick state
uint8_t joypin[JOYLAST];

// pins for analog inputs
uint8_t joypot[JOYMAXPOT];
uint8_t joypotnum = 0;

// current configuration
uint8_t config = 0;

/******************************
 *         Main code
 ******************************/

// Define Gotek LCD object
GotekTM1651 LCD(CLK_PIN, DIO_PIN);

// set digital or analog pin mode
void joyPinMode(uint8_t pin, uint8_t mode) {
  switch(mode) {
    case INPUT:
      pinMode(pin,mode);
      digitalWrite(pin,LOW);
      break;
    case INPUT_PULLUP:
      if (pin >= A0)
        pinMode(pin,INPUT);
      else
        pinMode(pin,mode);
      digitalWrite(pin,HIGH);
      break;
    case OUTPUT:
      pinMode(pin,mode);
  }
}

void setJoystickPins(uint8_t state) {
  uint8_t i, pos, pin;
  // clear current pin assignments
  for (i=0; i<JOYLAST; i++) joypin[i] = JOYNC;
  dbgprint("SETSTAT:");dbgprintln(state);
  // set new pin assignments
  for (i=0; i<9; i++) {
    pos = joyLine[state][i].pos;
    pin = joyLine[state][i].pin;
    joypin[pos] = pin;
    if (joyLine[state][i].alt) {
      joyPinMode(pin, joyLine[state][i].mode);
      if (joyLine[state][i].level != JOYNC)
        digitalWrite(pin, joyLine[state][i].level);
      dbgprint("SETPIN:");dbgprint(pin);dbgprint(" MOD:");dbgprint(joyLine[state][i].mode);dbgprint(" LEV:");dbgprintln(joyLine[state][i].level);
    }
  }
}

// setup a joystick from joyconfig array
void setupJoystick(uint8_t config) {
  const uint8_t *joy = joyconfig[config];
  uint8_t i, j, i9, pos, pin, state;
  boolean alt;

  // clear current line data
  for (i=0; i<JOYMAXSTATE; i++)
    for (j=0; j<9; j++) {
      joyLine[i][j].pin = JOYNC;
      joyLine[i][j].mode = JOYNC;
      joyLine[i][j].level = JOYNC;
      joyLine[i][j].pos = JOYNC;
      joyLine[i][j].alt = false;
    }

  // clear potentiometer data
  for (i=0; i<JOYMAXPOT; i++) joypot[i] = JOYNC;
  joypotnum = 0;

  // set new data
  i = -1;
  while (joy[++i] != JOYLAST) {
    state = i / 9;
    i9 = i % 9;
    pos = joy[i];
    joyLine[state][i9].pos = pos;
    dbgprint("I:"); dbgprint(i); dbgprint("\t");
    dbgprint("POS:"); dbgprint(pos); dbgprint("\t");
    dbgprint("ST:"); dbgprint(state); dbgprint("\t");
    if (pos != JOYNC) {
      pin = joyPinMap[i%9];
      joyLine[state][i9].pin = pin;
      joyLine[state][i9].alt = true;

      // check if we have potentiometer
      if (pos == JOYPOT) {
        joyLine[state][i9].mode = INPUT;
        joyLine[state][i9].level = LOW;
        joypot[joypotnum] = pin;
        dbgprint("POT:"); dbgprintln(pin);
        joypotnum++;
      }

      // check if the line is GND
      else if (pos == JOYGND) {
        joyLine[state][i9].mode = OUTPUT;
        joyLine[state][i9].level = LOW;
        dbgprint("GND:"); dbgprintln(pin);
      }

      // check if the line is VCC
      else if (pos == JOYVCC) {
        joyLine[state][i9].mode = OUTPUT;
        joyLine[state][i9].level = HIGH;
        dbgprint("VCC:"); dbgprintln(pin);
      }

      // any other line is input
      else {
        joyLine[state][i9].mode = INPUT_PULLUP;
        dbgprint("PIN:"); dbgprintln(pin);
      }
    }
  }

  // set pins for first state
  setJoystickPins(0);
  // determine number of states
  joyStates = i / 9;
  dbgprint("JOYSTATES:"); dbgprintln(joyStates);

  // check if there are alternating lines
  for (i=0; i<9; i++) {
    alt = false;
    for (j=1; j<joyStates; j++)
      if (joyLine[j-1][i].mode != joyLine[j][i].mode) alt = true;
      else if (joyLine[j-1][i].level != joyLine[j][i].level) alt = true;
    for (j=0; j<joyStates; j++)
      joyLine[j][i].alt = alt;
    if (alt) { dbgprint("ALT:"); dbgprintln(joyLine[0][i].pin); }
  }
  LCD.writeString(joyname[config]);
}

void setup() {
  #ifdef JOYDEBUG
    Serial.begin(9600);
  #endif
    Serial.begin(9600);
  dbgprintln("\n\n\nSETUP");

  // setup I2C for PCF8574
  Wire.begin();

  // setup buttons
  pinMode(BTN1_PIN, INPUT_PULLUP);
  pinMode(BTN2_PIN, INPUT_PULLUP);

  // setup Gotek LCD
  LCD.init();
  LCD.set(BRIGHT_TYPICAL);

  // setup initial configuration
  setupJoystick(config);
  dbgprintln("");
}

void loop() {
  // used for various timers
  unsigned long currLoopTime = millis();
  // loop variables
  uint8_t i,state;
  // detected bits on digital inputs
  uint8_t digital = 0;
  // button reading
  uint8_t buttonread;
  // analog reading
  long analog;
  // curent min and max from potbuff
  int buffmin, buffmax;

  // read button 1 state (based on arduino debounce example)
  buttonread = digitalRead(BTN1_PIN);
  if (buttonread != lastButton1State)
    lastDebounce1Time = millis();
  if ((millis() - lastDebounce1Time) > debounceDelay) {
    if (buttonread != button1State) {
      button1State = buttonread;
    // if button was pressed
    if (button1State == LOW) {
        if (btn1Mode == BTN1_CFGSEL) {
          // select next configuration
          config = (config + 1) % JOYCONFIGS;
          setupJoystick(config);
          dbgprintln("CFG SELECT MODE");
        }
        else {
          // change analog display mode
          potMode = (potMode == POT_LINE) ? POT_NUMBER : POT_LINE;
          dbgprintln("PADDLE MODE");
        }
      }
    }
  }
  lastButton1State = buttonread;

  // read button 2 state (based on arduino debounce example)
  buttonread = digitalRead(BTN2_PIN);
  if (buttonread != lastButton2State)
    lastDebounce2Time = millis();
  if ((millis() - lastDebounce2Time) > debounceDelay) {
    if (buttonread != button2State) {
      button2State = buttonread;
    // if button was pressed
    if (button2State == LOW) {
        // initialize analog read buffer
        for (i=0; i<POTBUFF; i++) potbuff[i] = -1;
        potbuffcnt = 0;
        joypotmin = 1023;
        joypotmax = 0;
        // select next potentiometer
        joypotcurr++;
        // set button 1 to analog display select mode
        btn1Mode = BTN1_POT;
        if ( (joypotcurr > JOYMAXPOT) || (joypot[joypotcurr-1] == JOYNC)) {
          // if there are no more potentiometers, return to cfg select mode
          joypotcurr = 0;
          btn1Mode = BTN1_CFGSEL;
          LCD.writeString(joyname[config]);
        }
        dbgprint("POT: ");dbgprintln(joypotcurr);
      }
    }
  }
  lastButton2State = buttonread;

  // check digital inputs
  for (state=0; state<joyStates; state++) {
    setJoystickPins(state);
    for (i=JOYUP; i<=JOYFIRE4; i++) {
      if (joypin[i] != JOYNC) {
        if (digitalRead(joypin[i]) == LOW)
          digital |= joybit[i-JOYUP];
          dbgprint("READPIN:"); dbgprint(joypin[i]); dbgprint(" POS:");dbgprint(i); dbgprint(":"); dbgprintln(digitalRead(joypin[i]));
      }
    }
  }

  // check potentiometers
  if (joypotcurr != 0) {
    if (currLoopTime - potReadTime > potReadInterval) {
      analog = analogRead(joypot[joypotcurr-1]);
      dbgprint("POT:");dbgprint(joypotcurr);dbgprint("\t");dbgprint("PIN:");dbgprint(joypot[joypotcurr-1]);dbgprint(" VAL:");dbgprintln(analog);
      // filter potentiometer values for line display
      potbuff[potbuffcnt] = analog;
      potbuffcnt = (potbuffcnt + 1) % POTBUFF;
      buffmin = buffmax = potbuff[0];
      for (i=1; i < POTBUFF; i++)
        if (potbuff[i] >= 0) {
          if (buffmin > analog) buffmin = analog;
          if (buffmax < analog) buffmax = analog;
        }
      // determine min and max values for line display
      if (joypotmin > buffmax) joypotmin = buffmax;
      if (joypotmax < buffmin) joypotmax = buffmin;
      if (potMode == POT_LINE) {
        if (joypotmax != joypotmin)
          // number of line segments
          analog = (analog - joypotmin + (joypotmax - joypotmin)/24) * 12 / (joypotmax - joypotmin);
        analog = abs(analog) % 13;
        LCD.writeData(lcdanalog[analog][0],lcdanalog[analog][1],lcdanalog[analog][2]);
        dbgprint("SEG:");dbgprint(analog);dbgprint(" MIN:");dbgprint(joypotmin);dbgprint(" MAX:");dbgprintln(joypotmax);
      }
      else {
        analog = analog * 999 / 1023;
        LCD.writeNumber(analog);
      }
      potReadTime = currLoopTime;
    }
    // blink fire3/4 LED to indicate selected analog input
    if (currLoopTime - potBlinkTime > potBlinkInterval) {
      potBlinkState = !potBlinkState;
      potBlinkTime = currLoopTime;
    }
    if (potBlinkState) {
      if (joypotcurr == 1)
        digital |= 0x40;
      else
        digital |= 0x80;
    }
  }

  // light up direction leds
  Wire.beginTransmission(0x20);
  Wire.write(~digital);
  Wire.endTransmission();

  dbgprintln("");
  #ifdef JOYDEBUG
    delay(100);
  #endif
}

