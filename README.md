# DB-9 Joystick Tester #

This project has started as a replacement for my old atari joystick tester that was realised with several LEDs and resistors, but could only test joysticks with atari compatible pinouts.
As it is my first Arduino project, I'm sure that more seasoned Arduino users would probably solve some thngs differently or better, but for now it is good enough for me.

The main interface consists of LEDs that show joystick directions and buttons, one 3-digit 7-segment display to show current configuration, two buttons to select configuration and paddles, switch for selecting 500K/1M resistance for analog inputs, 9 contacts for checking joystick cable wires and an on/off switch.

Old joystick tester was used to check joysticks for [Once Upon a Byte](http://www.onceuponabyte.org/) collection, and the new one has the same fate. Only now, we can check much more!

Onto the project...

### Parts list ###

Main parts:

* 1x Arduino in some form
* 1x DB-9 male connector
* 1x DIN 6 pol 240 connector (optional)
* 1x Gotek 3-digit display
* 2x buttons
* 1x 4P2T switch
* 1x 1P2T switch
* 8x 1M resistors
* 1x stripboard
* 9x small nails
* lots of wire
* a box to put it all in

Direction display:

* 1x PCF8574
* 1x 100nF ceramic capacitor
* 8x LEDs, 4 for directions, 4 for buttons
* 8x 470 Ohms resistors
* 2x 2.2K resistors

Power supply:

* 1x 7805
* 1x 100nf ceramic capacitor
* 1x 10uF electrolytic capacitor
* 1x connector for 9V battery
* 1x connector for external power supply

### Input ###

DB-9 connector on the joystick cable has the following pinout:

![DB-9 pinout](images/dsub9f.png)

For different systems, meaning of pins is different (see JoystickTester.ino for the list).
For the joysticks that I have found and tested, 5 pins are only digital across all configurations, while 4 can be analog or digital.
They are connected to 5 digital and 4 analog I/O pins on Arduino. Potentiometers on old controllers usually didn't have all 3 pins connected inside, so a voltage divider is needed to read them, as explained in [Atari Joystick Plus](https://github.com/nomdeclume/AtariJoysticksPlus/blob/master/AtariJoysticksPlus.ino).
As some joysticks/paddles had ~1M and some had ~500K potentiometers, I have added a switch to include 1M or 500K additional resestance:

![analog input](images/analoginput.png)

All analog inputs are connected to one 4P2T switch, so all can be set with 1M or 500K resistance simultaneously.

For this I have made a small board from stripboard:

![input board 1](images/inp1.jpg) ![input board 2](images/inp2.jpg)

Board is connected to DB-9 connector, 4P2T switch and Arduino digital and analog I/O pins:

![input board 3](images/inp3.jpg)

There is actually a 2P2T switch on the picture, as 4P2T hasn't yet arrived.

And just because there was enough room inside the box, I added a DIN connector, used for some consoles:

![DIN](images/din6pol.jpg)

### LED display ###

LEDs are connected to PCF8574, a circuit that has 8 I/O pins:

![PCF8574](images/pcf8574.png)

Each LED is connected to the VCC with 470 Ohm resistor and to one of the P0-P7 pins. I made a board that holds LEDs, PCF8574 and resistors with this layout:

![led board 1](images/led1.jpg) ![led board 2](images/led2.jpg)

Pins P0-P3 are for up, down, left and right, while P4-P7 are for fire buttons 1-4.
100nF ceramic capacitor is placed between VCC and GND and two 2.2K resistors are connected as pullups for SDA and SCL lines (as per requirements).
Address lines A0-A2 are connected to GND which gives I2C addres 0x20.
Board connects with one pair of wires to VCC/GND and with the other to Arduino's A4/A5 pins (used for I2C).

### Configuration display ###

For configuration display I choose 3-digit 7-segment display that was part of Gotek floppy emulator, as I had several laying around.
It uses TM1651 as a controller chip and I have adapted Fred Chu's existing TM1651 library for my use (I have also included parts of code from Keir Fraser's FlashFloppy project).

![TM1651](images/tm1651.png)

If you don't have these around (I have bacause I have started to replace all Gotek displays with OLEDs), you'll need to come up with some other solution for this.

![gotek 1](images/gotek1.jpg) ![gotek 2](images/gotek2.jpg)

Board pinout is on the second picture. Two pins are connected to VCC/GND, and oter two (DIO and CLK) are connected to arduino digital pins. Library GotekTM1651 is used to write to display.

This is used to display current joystick configuration (ATA for Atari/Commodore, CPC for Amstrad CPC and so on), and to display analog readings.

### Cotrols ###

First button is used for joystick configuration selection. Pressing it switches to next configuration, while configuration name is displayed on 3-digit display.

When a configuration contains potentiometers, second button is used to select which potentimeter is going to be read.
Blinking of Fire3 and Fire4 LEDs is used to signalize first or second potentiometer.

When a potentimeter is selected, 3-digit display shows potentiometer reading instead of configuration name.
During that time, first button can be used to select one of two ways to display analog value: numerical (direct reading of analog input scaled to 3 digits) or as an improvised line.
When line is selected, you must move potentiometer to its max and min positions, as only after that line will be synchronised with potentiometer movement.

Additionally, separate switch is used to select resistance of voltage divider, 500K or 1M.
You need to select the value that is nearer to the value of potentiometers inside joystick or paddle, to have a wider range of values.

9 contacts on the box in the shape of DB-9 connector are directly connected to its pins.
Their purpose it to easily check the state of wires inside joystick cable, using a multimeter or similar device (of course, this is to be used while repairing a joystick, as you need access to other end of the wire inside the joystick).

### Power supply ###

Power supply, consisting of 7805 and capacitors, is connected to connectors for 9V battery and external power supply on the input side, and to 5V power supply connector on the output side.

![7805](images/7805.png)

### Finished project ###

On the inside it looks like this:

![box1](images/box1.jpg)

and on the outside like this:

![box2](images/box2.jpg)

Video showing most of the features can be found on [Youtube](https://www.youtube.com/watch?v=Of8tIoZPjQs).

### Licence ###

All code is released under GNU GPL 2.0 or later licence.

